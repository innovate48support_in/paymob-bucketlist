package com.innovate.paymob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymobBucketlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymobBucketlistApplication.class, args);
	}

}
